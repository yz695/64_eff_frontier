Grading at 11/26/2021:16:29:28.713351
For commit ID b423dc0b22c804a4e899e3c0820b858adec54c7e
Grading at Fri Nov 26 11:28:59 EST 2021
rm -f *~ *.o efficient_frontier
g++ -o efficient_frontier -std=gnu++11 -Wall -Werror -ggdb3 -Os main.cpp 
+-----------------------------------------------------------------------+
Test case 1
Error: Number of parameters is wrong.: Success
Your program successfully indicated a failure case
                                                         [2/2]
+-----------------------------------------------------------------------+
Test case 2
ROR,volatility
1.0%,0.78%
2.0%,1.09%
3.0%,1.59%
4.0%,2.13%
5.0%,2.71%
6.0%,3.29%
7.0%,3.88%
8.0%,4.47%
9.0%,5.07%
10.0%,5.67%
11.0%,6.27%
12.0%,6.86%
13.0%,7.46%
14.0%,8.06%
15.0%,8.67%
16.0%,9.27%
17.0%,9.87%
18.0%,10.47%
19.0%,11.07%
20.0%,11.67%
21.0%,12.28%
22.0%,12.88%
23.0%,13.48%
24.0%,14.08%
25.0%,14.69%
26.0%,15.29%
Your program did not indicate a failure case
                                                         [0/2]
+-----------------------------------------------------------------------+
Test case 3
Error: File name format is wrong.: Success
Your program successfully indicated a failure case
                                                         [2/2]
+-----------------------------------------------------------------------+
Test case 4
Error: File name format is wrong.: Success
Your program successfully indicated a failure case
                                                         [2/2]
+-----------------------------------------------------------------------+
Test case 5
ROR,volatility
1.0%,0.79%
2.0%,1.17%
3.0%,1.74%
4.0%,2.37%
5.0%,3.02%
6.0%,3.67%
7.0%,4.34%
8.0%,5.00%
9.0%,5.67%
10.0%,6.34%
11.0%,7.01%
12.0%,7.68%
13.0%,8.35%
14.0%,9.03%
15.0%,9.70%
16.0%,10.37%
17.0%,11.05%
18.0%,11.72%
19.0%,12.39%
20.0%,13.07%
21.0%,13.74%
22.0%,14.42%
23.0%,15.09%
24.0%,15.76%
25.0%,16.44%
26.0%,17.11%
Your program did not indicate a failure case
                                                         [0/4]
+-----------------------------------------------------------------------+
Test case 6
ROR,volatility
1.0%,0.11%
2.0%,0.75%
3.0%,1.37%
4.0%,1.98%
5.0%,2.59%
6.0%,3.20%
7.0%,3.81%
8.0%,4.41%
9.0%,5.02%
10.0%,5.63%
11.0%,6.24%
12.0%,6.85%
13.0%,7.46%
14.0%,8.07%
15.0%,8.68%
16.0%,9.29%
17.0%,9.90%
18.0%,10.51%
19.0%,11.11%
20.0%,11.72%
21.0%,12.33%
22.0%,12.94%
23.0%,13.55%
24.0%,14.16%
25.0%,14.77%
26.0%,15.38%
Valgrind reported memory errors
Your program did not indicate a failure case
                                                         [0/4]
+-----------------------------------------------------------------------+
Test case 7
Valgrind reported memory errors
Your program did not indicate a failure case
    But your program did exit failure
                                                         [1/4]
+-----------------------------------------------------------------------+
Test case 8
Error: Please check your input, it should be number!
: Success
Your program successfully indicated a failure case
                                                         [4/4]
+-----------------------------------------------------------------------+
Test case 9
Error: Please check your input, it should be number!
: Success
Your program successfully indicated a failure case
                                                         [4/4]
+-----------------------------------------------------------------------+
Test case 10
ROR,volatility
1.0%,0.79%
2.0%,1.15%
3.0%,1.71%
4.0%,2.32%
5.0%,2.96%
6.0%,3.60%
7.0%,4.25%
8.0%,4.91%
9.0%,5.56%
10.0%,6.22%
11.0%,6.88%
12.0%,7.54%
13.0%,8.20%
14.0%,8.86%
15.0%,9.52%
16.0%,10.18%
17.0%,10.84%
18.0%,11.50%
19.0%,12.16%
20.0%,12.82%
21.0%,13.48%
22.0%,14.14%
23.0%,14.81%
24.0%,15.47%
25.0%,16.13%
26.0%,16.79%
Your program did not indicate a failure case
                                                         [0/2]
+-----------------------------------------------------------------------+
Test case 11
Your file matched the expected output
                                                         [40/40]
+-----------------------------------------------------------------------+
Test case 12
Your file matched the expected output
                                                         [5/5]
+-----------------------------------------------------------------------+
Test case 13
Your file matched the expected output
                                                         [5/5]
+-----------------------------------------------------------------------+
Test case 14
Your file matched the expected output
                                                         [5/5]
+-----------------------------------------------------------------------+
Test case 15
Your file matched the expected output
                                                         [5/5]
========================================================================
Restricted test cases for bonus points
========================================================================
+-----------------------------------------------------------------------+
Test case 16
Your file matched the expected output
                                                         [10/10]
+-----------------------------------------------------------------------+
Test case 17
Your file matched the expected output
                                                         [5/5]
+-----------------------------------------------------------------------+
Code quality
                                                         [10/10]
+-----------------------------------------------------------------------+

Overall Grade: 100
