#ifndef __PORTFOLIO_H__
#define __PORTFOLIO_H__

#include <string>
#include <vector>
#include "asset.hpp"
using namespace std;

class Portfolio{
public:
    double rate_of_raturn;
    double volatility;
    void optimize(MatrixXd& covarianceMatrix, MatrixXd& A, VectorXd& b, vector<Asset>& assetVector, bool restrictedFlag);
};

#endif