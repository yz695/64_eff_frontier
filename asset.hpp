#ifndef __ASSET_H__
#define __ASSET_H__

#include <string>
#include <vector>
using namespace std;

class Asset{
public:
    bool deleted = false;
    string name;
    double standardDeviation;
    double expectedReturn;
};

#endif