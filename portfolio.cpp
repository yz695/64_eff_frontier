#include <string>
#include <vector>
#include <Eigen/Dense>
#include "asset.hpp"
#include "portfolio.hpp"

using namespace std;
using Eigen::MatrixXd;
using Eigen::VectorXd;

void removeRowCol(MatrixXd& matrix, int target){
    matrix.block(0,target,matrix.rows(),matrix.cols()-1-target) = matrix.block(0,target+1,matrix.rows(),matrix.cols()-1-target);
    matrix.conservativeResize(matrix.rows(),matrix.cols()-1);
    matrix.block(target,0,matrix.rows()-1-target,matrix.cols()) = matrix.block(target+1,0,matrix.rows()-1-target,matrix.cols());
    matrix.conservativeResize(matrix.rows()-1,matrix.cols());
}

void removeVecRow(VectorXd& vector, int target){
    vector.block(target,0,vector.rows()-1-target,vector.cols()) = vector.block(target+1,0,vector.rows()-1-target,vector.cols());
    vector.conservativeResize(vector.rows()-1);
}

void Portfolio::optimize(MatrixXd& covarianceMatrix, MatrixXd& A, VectorXd& b, vector<Asset>& assetVector, bool restrictedFlag){
    /* ------------------------- set vector b ------------------------- */
    b(assetVector.size()) = 1;
    b(assetVector.size()+1) = this->rate_of_raturn;
    
    /* ------------------------- calculate X ------------------------- */
    VectorXd XLamda(assetVector.size() + 2);
    XLamda = A.colPivHouseholderQr().solve(b);

    VectorXd X(assetVector.size());
    X = XLamda.head(assetVector.size()); 


    /* ------------------------- restricted ------------------------- */
    if(restrictedFlag){
        MatrixXd tempA = A;
        VectorXd tempB = b;
        bool allPositive = false;
        while (!allPositive){
            //calculate X with lamda
            XLamda = tempA.colPivHouseholderQr().solve(tempB);
            //get X without lamda
            X = XLamda.head(XLamda.rows()-2); //cite https://eigen.tuxfamily.org/dox/group__TutorialBlockOperations.html

            allPositive = true;
            
            size_t position=0;
            for (size_t i = 0; i < assetVector.size(); i++){
                if(assetVector[i].deleted){
                    continue;
                }
                else{
                    if(X(position) < 0){
                    allPositive = false;
                    assetVector[i].deleted = true;
                    removeVecRow(tempB,position);
                    removeVecRow(XLamda,position);
                    removeVecRow(X,position);
                    removeRowCol(tempA,position);
                    }
                    else{
                        position++;
                    }
                }
                
            }

        }                        
    }

    vector<double> XVector;
    if(restrictedFlag==true){
        size_t position = 0;

        for (size_t i = 0; i < assetVector.size(); i++){
            if(assetVector[i].deleted == true){
                XVector.push_back(0);
                assetVector[i].deleted = false;
            }
            else{
                XVector.push_back(X(position++));
            }
        }
    }
    else{
        for (size_t i = 0; i < assetVector.size(); i++){
            XVector.push_back(X(i));
        }
    }

    /* ------------------------- get variance ------------------------- */
    double sumVariance = 0;
    for (size_t i = 0; i < assetVector.size(); i++){
        for (size_t j = 0; j < assetVector.size(); j++){
            sumVariance += XVector[i]*XVector[j]*covarianceMatrix(i,j);
        }
    }

    /* ------------------------- get volatility ------------------------- */
    this->volatility = sqrt(sumVariance);
}
