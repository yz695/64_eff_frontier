#include <string>
#include <vector> 
#include <string.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <unistd.h>
#include <Eigen/Dense>
#include "asset.hpp"
#include "parse.cpp"
#include "portfolio.hpp"
#include "portfolio.cpp"

using namespace std;
using Eigen::MatrixXd;
using Eigen::VectorXd;

bool checkFileFormat(char * name){
    int l = strlen(name);
    if(name[l-4]=='.'&&name[l-3]=='c'&&name[l-2]=='s'&&name[l-1]=='v'){
        return true;
    }
    else{
        return false;
    }
}

int main(int argc,char *argv[]){
    /* ------------------------- Verify the number of parameters ------------------------- */
    if(!(argc>=3&&argc<=4)){
        perror("Error: Number of parameters is wrong.");
        exit(EXIT_FAILURE);
    }
    
    /* ------------------------- Get option ------------------------- */
    int opt;
    bool restrictedFlag = false;

    while ((opt = getopt(argc, argv, "r")) != -1){
        switch (opt){
            case '?':
                perror("Error: Unknown option.");
                exit(EXIT_FAILURE);
                break;

            case 'r':
                restrictedFlag = true;
                break;
        }
    }
    
    /* ------------------------- Check file name format ------------------------- */
    if(!checkFileFormat(argv[optind])||!checkFileFormat(argv[optind+1])){
        perror("Error: File name format is wrong.");
        exit(EXIT_FAILURE);
    }

    /* ------------------------- Read File and convert into Matrix and Vector ------------------------- */    
    vector<Asset> assetVector;
    fileParseAsset(argv[optind], assetVector);

    // MatrixXd correlationMatrix(assetVector.size(),assetVector.size());
    vector<vector<double>> correlationMatrix;
    fileParseCorrelation(argv[optind+1], correlationMatrix, assetVector.size());

    /* ------------------------- calculate covariance matrix ------------------------- */
    MatrixXd covarianceMatrix(assetVector.size(),assetVector.size());
    for (size_t i = 0; i < assetVector.size(); i++) {
        for (size_t j = 0; j < assetVector.size(); j++) {
            covarianceMatrix(i,j) = correlationMatrix[i][j]*assetVector[i].standardDeviation*assetVector[j].standardDeviation;
        }
    }

    /* ------------------------- calculate Matrix A ------------------------- */
    MatrixXd A(assetVector.size()+2,assetVector.size()+2);
    // A = MatrixXd::Zero(assetVector.size()+2,assetVector.size()+2);
    for (size_t i = 0; i < assetVector.size(); i++) {
        for (size_t j = 0; j < assetVector.size(); j++) {
            A(i,j) = covarianceMatrix(i,j);
        }
    }
    for (size_t i = 0; i < assetVector.size(); i++){
        A(i,assetVector.size()) = 1;
        A(assetVector.size(),i) = 1;
    }
    for (size_t i = 0; i < assetVector.size()+1; i++){
        A(i,assetVector.size()+1) = assetVector[i].expectedReturn;
        A(assetVector.size()+1,i) = assetVector[i].expectedReturn;
    }
    for (size_t i = assetVector.size(); i <= assetVector.size()+1; i++){
        for (size_t j = assetVector.size(); j <= assetVector.size()+1; j++){
            A(i,j) = 0;
        }
    }


    /* ------------------------- create vector b ------------------------- */
    VectorXd b(assetVector.size()+2);
    b = VectorXd::Zero(assetVector.size()+2);

    /* ------------------------- calculate volatility ------------------------- */
    vector<Portfolio> portfolioVec;
    for (size_t i = 1; i <= 26; i++) {
        Portfolio tempPort;
        tempPort.rate_of_raturn = i * 0.01;
        tempPort.optimize(covarianceMatrix,A,b,assetVector,restrictedFlag);
        portfolioVec.push_back(tempPort);
    }
    
    /* ------------------------- output result ------------------------- */
    cout << "ROR,volatility" << endl;
    for (size_t i = 0; i < 26; i++) {
        cout << setiosflags(ios::fixed)<<setprecision(1);
        cout << portfolioVec[i].rate_of_raturn * 100 << "%,";
        cout << setiosflags(ios::fixed)<<setprecision(2);
        cout << portfolioVec[i].volatility*100 << "%" <<endl;
    }
}
