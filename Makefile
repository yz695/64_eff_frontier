CFLAGS=-std=gnu++11 -Wall -Werror -ggdb3 -Os
efficient_frontier: asset.hpp main.cpp parse.cpp portfolio.hpp portfolio.cpp
	g++ -o efficient_frontier $(CFLAGS) main.cpp 

.PHONY: clean
clean:
	rm -f *~ *.o efficient_frontier
