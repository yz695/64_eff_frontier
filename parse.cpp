#include <iostream>
#include <vector>
#include <sstream>
#include <string.h>
#include <string>
#include <fstream>
#include <Eigen/Dense>
#include "asset.hpp"
using namespace std;
using Eigen::MatrixXd;
using Eigen::VectorXd;

void fileParseAsset(char* fileName,vector<Asset>& assetVec){
    string line;
    string tempStr;
    Asset tempAsset;
    double tempNum;
    ifstream inFile;
    inFile.open(fileName,ios::in);
    if (!inFile){
        perror("Read file error");
        exit(EXIT_FAILURE);
    }

    while (getline(inFile,line)){ 
        istringstream sin(line);

        /* ------------------------- Name ------------------------- */
        getline(sin, tempStr, ','); 
        tempAsset.name = tempStr;

        /* ------------------------- expectedReturn ------------------------- */
        getline(sin, tempStr, ','); 
        if(isdigit(tempStr[tempStr.size()-1])){
            tempNum = stod(tempStr);
            tempAsset.expectedReturn = tempNum;
        }
        else{
            perror("Error: Please check your input, it should be number!\n");
            exit(EXIT_FAILURE);
        }


        /* ------------------------- standardDeviation ------------------------- */
        getline(sin, tempStr, ','); 
        if(isdigit(tempStr[tempStr.size()-1])){
            tempNum = stod(tempStr);
            tempAsset.standardDeviation = tempNum;
        }
        else{
            perror("Error: Please check your input, it should be number!\n");
            exit(EXIT_FAILURE);
        }

        assetVec.push_back(tempAsset);
    }
}

void fileParseCorrelation(char* fileName, vector<vector<double>>& correlationMatrix, int assetNum){
    string line;
    string tempStr;
    double tempNum;
    ifstream inFile;
    inFile.open(fileName,ios::in);
    if (!inFile){
        perror("Read file error");
        exit(EXIT_FAILURE);
    }

    while (getline(inFile,line)){ 
        istringstream sin(line);
        vector<double> tempVector;
        while(getline(sin, tempStr, ',')){
            if(isdigit(tempStr[tempStr.size()-1])){
                tempNum = stod(tempStr);
                tempVector.push_back(tempNum);
            }
            else{
                perror("Error: Please check your input, it should be number!\n");
                exit(EXIT_FAILURE);
            }
        }
        correlationMatrix.push_back(tempVector);
    }
}